package com.example.onboard2.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.example.onboard2.R;


public class termOfUseFragment extends Fragment {

    public static final String TAG = termOfUseFragment.class.getSimpleName();

    public termOfUseFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static termOfUseFragment newInstance() {
        return new termOfUseFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_term_of_use, container, false);

        //mUnbinder = ButterKnife.bind(this, view);

        return view;
    }
}