package com.example.onboard2.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.onboard2.BuildConfig;
import com.example.onboard2.FriendList;
import com.example.onboard2.R;
import com.example.onboard2.TermOfUse;
import com.example.onboard2.gsonmodels.LoginModel;
import com.example.onboard2.utils.NetworkHelper;
import com.example.onboard2.utils.SharedPreferenceHelper;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.GsonBuilder;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import timber.log.Timber;


public class loginFragment extends Fragment {

    public static final String TAG = loginFragment.class.getSimpleName();
    public boolean nullemail;
    public boolean nullpassword;
    public boolean passwordCorrect;

    @BindView(R.id.btn_login)
    protected Button loginButton;

    @BindView(R.id.txt_tnc)
    protected TextView termOfUse;


    @BindView(R.id.edittext_email)
    protected TextInputEditText mEditTextEmail;

    @BindView(R.id.edittext_password)
    protected TextInputEditText mEditTextPasword;

    private Unbinder mUnbinder;

    public loginFragment() {
        // Required empty public constructor
    }


    public static loginFragment newInstance() {
        return new loginFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (BuildConfig.DEBUG) {
            // Auto fill in credential for testing purpose.
            mEditTextEmail.setText("eve.holt@reqres.in");
            mEditTextPasword.setText("cityslicka");
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }


    @OnClick(R.id.btn_login)
    protected void doLogin() {
        loginButton.setEnabled(false);
        loginButton.setText("Loading");
        final String email = mEditTextEmail.getText().toString().trim();
        final String password = mEditTextPasword.getText().toString();
        Toast.makeText(getActivity(), "Please wait while login.", Toast.LENGTH_SHORT).show();
        if (mEditTextEmail.getText().toString().trim().length() == 0) {
            nullemail = true;
        } else if (mEditTextEmail.getText().toString().trim().length() != 0) {
            nullemail = false;
        }
        if (mEditTextPasword.getText().toString().length() == 0) {
            nullpassword = true;
        } else if (mEditTextPasword.getText().toString().length() != 0) {
            nullpassword = false;
        }

        String url = BuildConfig.BASE_API_URL + "login";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                if (getLifecycle().getCurrentState().toString().compareToIgnoreCase("resume") == 1) {


                    if (nullemail == true) {
                        new MaterialDialog.Builder(getActivity())
                                .title("Empty Email")
                                .content("Email cannot be blank.")
                                .positiveText("OK")
                                .show();

                    } else if (nullpassword == true) {
                        new MaterialDialog.Builder(getActivity())
                                .title("Empty Password")
                                .content("Password cannot be blank.")
                                .positiveText("OK")
                                .show();

                    } else if (nullemail == true && nullpassword == true) {
                        new MaterialDialog.Builder(getActivity())
                                .title("None Values Found!")
                                .content("Email and Password cannot left blank..")
                                .positiveText("OK")
                                .show();

                    } else if (!(mEditTextPasword.getText().toString().equals(password.toString()))) {
                        new MaterialDialog.Builder(getActivity())
                                .title("Error")
                                .content("Invalid email or password.")
                                .positiveText("OK")
                                .show();

                    } else {
                        Toast.makeText(getActivity(), "Login success.", Toast.LENGTH_SHORT).show();
                        LoginModel responseModel = new GsonBuilder().create().fromJson(response, LoginModel.class);


                        SharedPreferenceHelper.getInstance(getActivity())
                                .edit()
                                .putString("email", email)
                                .putString("token", responseModel.token)
                                .apply();
                    }
                    startActivity(new Intent(getActivity(), FriendList.class));
                    getActivity().finish();

                }


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Timber.e("d;;Login error: %s", error.getMessage());
                if (nullemail == true) {
                    new MaterialDialog.Builder(getActivity())
                            .title("Empty Email")
                            .content("Email cannot be blank.")
                            .positiveText("OK")
                            .show();

                } else if (nullpassword == true) {
                    new MaterialDialog.Builder(getActivity())
                            .title("Empty Password")
                            .content("Password cannot be blank.")
                            .positiveText("OK")
                            .show();

                } else if (nullemail == true && nullpassword == true) {
                    new MaterialDialog.Builder(getActivity())
                            .title("None Values Found!")
                            .content("Email and Password cannot left blank..")
                            .positiveText("OK")
                            .show();

                } else if (!(mEditTextPasword.getText().toString().equals(password.toString()))) {
                    new MaterialDialog.Builder(getActivity())
                            .title("Error")
                            .content("Invalid email or password.")
                            .positiveText("OK")
                            .show();

                } else {
                    new MaterialDialog.Builder(getActivity())
                            .title("Error")
                            .content("Invalid email or password.")
                            .positiveText("OK")
                            .show();

                }
                loginButton.setEnabled(true);
                loginButton.setText("Login");
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("email", email);
                params.put("password", password);

                return params;
            }
        };

        NetworkHelper.getRequestQueueInstance(getActivity()).add(stringRequest);

    }


    @OnClick(R.id.txt_tnc)
    protected void goTerm() {


        startActivity(new Intent(getActivity(), TermOfUse.class));
        getActivity().finish();
    }


}