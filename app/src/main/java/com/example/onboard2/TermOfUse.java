package com.example.onboard2;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.onboard2.fragments.termOfUseFragment;

public class TermOfUse extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_termofuse);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .setReorderingAllowed(true)
                    .add(R.id.termofuse_holder, termOfUseFragment.class, null)
                    .commit();
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}