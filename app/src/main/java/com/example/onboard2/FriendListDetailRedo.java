package com.example.onboard2;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class FriendListDetailRedo extends AppCompatActivity {

    ImageView mainImageView;
    TextView firstname, lastname, email;
    String data1, data2, data3;
    int myImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_list_detail_redo);

        mainImageView = findViewById(R.id.image_redo);
        firstname = findViewById(R.id.first_redo);
        lastname = findViewById(R.id.last_redo);
        email = findViewById(R.id.email_redo);

        getData();
        setData();
    }

    private void getData() {
        if (getIntent().hasExtra("myImage") && getIntent().hasExtra("data1") && getIntent().hasExtra("data2") && getIntent().hasExtra("data3")) {
            data1 = getIntent().getStringExtra("data1");
            data2 = getIntent().getStringExtra("data2");
            data3 = getIntent().getStringExtra("data3");
            myImage = getIntent().getIntExtra("myImage", 1);

        } else {
            Toast.makeText(this, "No Data", Toast.LENGTH_SHORT).show();
        }

    }

    private void setData() {
        firstname.setText(data1);
        lastname.setText(data2);
        email.setText(data3);
        mainImageView.setImageResource(myImage);
    }
}