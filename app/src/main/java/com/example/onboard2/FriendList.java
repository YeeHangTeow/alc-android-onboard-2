package com.example.onboard2;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.onboard2.adapters.MyFriendAdapter;
import com.example.onboard2.utils.SharedPreferenceHelper;

public class FriendList extends AppCompatActivity {

    String namef[], namel[], emailfl[];
    int imgfl[] = {R.drawable.profile1, R.drawable.profile2};
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_list);
        namef = getResources().getStringArray(R.array.First_Name);
        namel = getResources().getStringArray(R.array.Last_Name);
        emailfl = getResources().getStringArray(R.array.Email);
        recyclerView = findViewById(R.id.recycleviewredo);

        MyFriendAdapter myfriendadapter = new MyFriendAdapter(this, namef, namel, emailfl, imgfl);
        recyclerView.setAdapter(myfriendadapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


    }


    @Override
    public boolean onCreateOptionsMenu(@NonNull Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.logout, menu);

        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.log_out) {
            new MaterialDialog.Builder(this)
                    .title("Logout")
                    .content("Are you sure to log Out ? ")
                    .positiveText("Yes")
                    .negativeText("No")
                    .onPositive((dialog, which) -> {
                        SharedPreferenceHelper.getInstance(this)
                                .edit()
                                .clear()
                                .apply();
                        startActivity(new Intent(this, Login.class));
                        this.finish();
                    })
                    .show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}