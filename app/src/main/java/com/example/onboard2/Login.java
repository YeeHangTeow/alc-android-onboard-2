package com.example.onboard2;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.example.onboard2.fragments.loginFragment;

import butterknife.ButterKnife;

public class Login extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);

        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.findFragmentByTag(loginFragment.TAG) == null) {
            fragmentManager.beginTransaction()
                    .replace(R.id.login_frame_holder, loginFragment.newInstance(), loginFragment.TAG)
                    .commit();

        }

    }
}